class Person {
    private name: string;
    private age: number;

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }

    displayDetails() {
        console.log(`Name: ${this.name}, Age: ${this.age}`);
    }
}

function createAndDisplayPerson() {
    const person = new Person("John", 30);
    person.displayDetails();
}

createAndDisplayPerson();
